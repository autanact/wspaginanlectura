
/**
 * WSpaginaNlecturaServiceMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */
        package co.net.une.www.svc;

        /**
        *  WSpaginaNlecturaServiceMessageReceiverInOut message receiver
        */

        public class WSpaginaNlecturaServiceMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver{


        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
        throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(msgContext);

        WSpaginaNlecturaServiceSkeleton skel = (WSpaginaNlecturaServiceSkeleton)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJava(op.getName().getLocalPart())) != null)){

        

            if("obtenerPaginaNLectura".equals(methodName)){
                
                co.net.une.www.gis.WSpaginaNlecturaRS wSpaginaNlecturaRS5 = null;
	                        co.net.une.www.gis.WSpaginaNlecturaRQ wrappedParam =
                                                             (co.net.une.www.gis.WSpaginaNlecturaRQ)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    co.net.une.www.gis.WSpaginaNlecturaRQ.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               wSpaginaNlecturaRS5 =
                                                   
                                                   
                                                           wrapobtenerPaginaNLectura(
                                                       
                                                        

                                                        
                                                       skel.obtenerPaginaNLectura(
                                                            
                                                                getNombreDepartamento(wrappedParam)
                                                            ,
                                                                getNombreMunicipio(wrappedParam)
                                                            ,
                                                                getCodigoDireccion(wrappedParam)
                                                            ,
                                                                getDireccionNatural(wrappedParam)
                                                            )
                                                    
                                                         )
                                                     ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), wSpaginaNlecturaRS5, false);
                                    
            } else {
              throw new java.lang.RuntimeException("method not found");
            }
        

        newMsgContext.setEnvelope(envelope);
        }
        }
        catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }
        
        //
            private  org.apache.axiom.om.OMElement  toOM(co.net.une.www.gis.WSpaginaNlecturaRQ param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(co.net.une.www.gis.WSpaginaNlecturaRQ.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(co.net.une.www.gis.WSpaginaNlecturaRS param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(co.net.une.www.gis.WSpaginaNlecturaRS.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, co.net.une.www.gis.WSpaginaNlecturaRS param, boolean optimizeContent)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(co.net.une.www.gis.WSpaginaNlecturaRS.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    

                        private java.lang.String getNombreDepartamento(
                        co.net.une.www.gis.WSpaginaNlecturaRQ wrappedType){
                        
                                return wrappedType.getWSpaginaNlecturaRQ().getNombreDepartamento();
                            
                        }
                     

                        private java.lang.String getNombreMunicipio(
                        co.net.une.www.gis.WSpaginaNlecturaRQ wrappedType){
                        
                                return wrappedType.getWSpaginaNlecturaRQ().getNombreMunicipio();
                            
                        }
                     

                        private java.lang.String getCodigoDireccion(
                        co.net.une.www.gis.WSpaginaNlecturaRQ wrappedType){
                        
                                return wrappedType.getWSpaginaNlecturaRQ().getCodigoDireccion();
                            
                        }
                     

                        private java.lang.String getDireccionNatural(
                        co.net.une.www.gis.WSpaginaNlecturaRQ wrappedType){
                        
                                return wrappedType.getWSpaginaNlecturaRQ().getDireccionNatural();
                            
                        }
                     
                        private co.net.une.www.gis.WSpaginaNlecturaRQType getobtenerPaginaNLectura(
                        co.net.une.www.gis.WSpaginaNlecturaRQ wrappedType){
                            return wrappedType.getWSpaginaNlecturaRQ();
                        }
                        
                        
                    
                         private co.net.une.www.gis.WSpaginaNlecturaRS wrapobtenerPaginaNLectura(
                            co.net.une.www.gis.WSpaginaNlecturaRSType innerType){
                                co.net.une.www.gis.WSpaginaNlecturaRS wrappedElement = new co.net.une.www.gis.WSpaginaNlecturaRS();
                                wrappedElement.setWSpaginaNlecturaRS(innerType);
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (co.net.une.www.gis.WSpaginaNlecturaRQ.class.equals(type)){
                
                           return co.net.une.www.gis.WSpaginaNlecturaRQ.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (co.net.une.www.gis.WSpaginaNlecturaRS.class.equals(type)){
                
                           return co.net.une.www.gis.WSpaginaNlecturaRS.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    

        /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
        private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
        org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
        returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
        return returnMap;
        }

        private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

        }//end of class
    