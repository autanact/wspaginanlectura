
/**
 * WSpaginaNlecturaServiceSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.3  Built on : Aug 10, 2007 (04:45:47 LKT)
 * with extensions for GE Smallworld GeoSpatial Server
 */
    package co.net.une.www.svc;
    import java.util.Map;
    import java.util.HashMap;    
    
    import org.apache.axis2.engine.AxisError;
    
    import com.gesmallworld.gss.lib.exception.GSSException;
    import com.gesmallworld.gss.webservice.WebServiceRequest;
    /**
     *  WSpaginaNlecturaServiceSkeleton java skeleton for the axisService
     */
    public class WSpaginaNlecturaServiceSkeleton extends WebServiceRequest
        {
        
	
	private static final String serviceName = "ejb/WSpaginaNlecturaServiceLocal";
	
     
         
        /**
         * Auto generated method signature
         
         
                                     * @param nombreDepartamento
                                     * @param nombreMunicipio
                                     * @param codigoDireccion
                                     * @param direccionNatural
         */
        

                 public co.net.une.www.gis.WSpaginaNlecturaRSType obtenerPaginaNLectura
                  (
                  java.lang.String nombreDepartamento,java.lang.String nombreMunicipio,java.lang.String codigoDireccion,java.lang.String direccionNatural
                  )
            {
                //GSS generated code
		Map<String,Object> params = new HashMap<String,Object>();
                  params.put("nombreDepartamento",nombreDepartamento);params.put("nombreMunicipio",nombreMunicipio);params.put("codigoDireccion",codigoDireccion);params.put("direccionNatural",direccionNatural);
		try{
		
			return (co.net.une.www.gis.WSpaginaNlecturaRSType)
			this.makeStructuredRequest(serviceName, "obtenerPaginaNLectura", params);
		}catch(GSSException e){
                    // Modify if specific faults are required
                    throw new AxisError(e.getLocalizedMessage()+": "+e.getRootThrowable().getLocalizedMessage(), e.getRootThrowable());
                }
        }
     
    }
    